package dev.belikov.karaulApp.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

data class PickedContact(
    val name: String?,
    val lastName: String?,
    val phoneNumber: String,
    val email: String?
)

class ContactPicker constructor(private val requestCode: Int = 23) : Fragment() {

    private lateinit var onContactPicked: (PickedContact) -> Unit
    private lateinit var onFailure: (Throwable) -> Unit

    companion object {

        private const val TAG = "ContactPicker"

        fun create(
            activity: AppCompatActivity,
            onContactPicked: (PickedContact) -> Unit,
            onFailure: (Throwable) -> Unit
        ): ContactPicker? {

            return try {
                val picker = ContactPicker()
                picker.onContactPicked = onContactPicked
                picker.onFailure = onFailure
                activity.supportFragmentManager.beginTransaction()
                    .add(picker, TAG)
                    .commitNowAllowingStateLoss()
                picker
            } catch (e: Exception) {
                onFailure(e)
                null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return null
    }

    fun pick() {
        try {
            Intent().apply {
                data = ContactsContract.Contacts.CONTENT_URI
                action = Intent.ACTION_PICK
                startActivityForResult(this, requestCode)
            }
        } catch (e: Exception) {
            onFailure(e)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                this.requestCode -> {
                    if (data?.data == null) return
                    contactPicked(data.data!!)
                }
            }
        }
    }


    private fun contactPicked(uri: Uri) {
        val cr = activity?.contentResolver ?: return
        var cur = cr.query(ContactsContract.Data.CONTENT_URI, null, null, null, null)
        cur?.moveToFirst()
        try {
            cur = activity?.contentResolver!!.query(
                ContactsContract.Data.CONTENT_URI,
                arrayOf(
                    ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
                    ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                    ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                ),
                ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.IN_VISIBLE_GROUP + " = ?",
                arrayOf(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, "1"),
                null
            ) ?: return
            cur.moveToFirst()
            val id =
                cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID))
            val name =
                cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME))
            val lastName =
                cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME))
            val pCur = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                arrayOf(id), null
            )
            var phone = ""
            while (pCur?.moveToNext() == true) {
                phone = pCur.getString(
                    pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                )
                phone = refactorPhone(phone)
            }
            pCur?.close()
            val emailCur = cr.query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                arrayOf(id), null
            )
            var email = ""
            if (emailCur != null) {
                while (emailCur.moveToNext()) {
                    email = emailCur.getString(
                        emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)
                    )
                }
            }
            emailCur?.close()

            onContactPicked(PickedContact(name, lastName, phone, email))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun refactorPhone(phone: String): String {
        val re = Regex("[^0-9]")
        return re.replace(phone, "").substring(1)

    }
}