package dev.belikov.karaulApp.sendEmail

import android.content.Context
import android.util.Log
import com.orhanobut.hawk.Hawk
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.location.LocationHelper
import dev.belikov.karaulApp.location.LocationInterface
import dev.belikov.karaulApp.location.LocationManager
import dev.belikov.karaulApp.network.RetrofitClient
import dev.belikov.karaulApp.utils.Const
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SendEmailHelper(val context: Context) : LocationInterface {

    private var locationHelper: LocationHelper? = null
    private var timer: Timer? = null
    fun sendEmail(from: String) {
        if (Hawk.get(Const.SEND_EMAIL, false)) {
            locationHelper =
                LocationManager.getInstance(context.applicationContext).locationHelper
            locationHelper?.getCurrentLocation(this, from)
        }
    }

    fun sendEmailTimer() {
        if (timer != null) {
            timer!!.purge()
            timer!!.cancel()
            timer = null
        } else {
            timer = Timer()
            timer!!.schedule(object : TimerTask() {
                override fun run() {
                    if (Hawk.get(Const.SEND_EMAIL, false)) {
                        locationHelper =
                            LocationManager.getInstance(context.applicationContext).locationHelper
                        locationHelper?.getCurrentLocation(this@SendEmailHelper)
                    }
                }
            }, 0, 30 * 1000L)
        }
    }

    fun checkTimer() = timer != null

    override fun sendEmail(location: String, from: String) {
        val yourName = Hawk.get(Const.HUMAN_NAME, "")
        val yourLastName = Hawk.get(Const.HUMAN_LAST_NAME, "")
        val yourFullName = "$yourName $yourLastName"
        val emergencyName = Hawk.get(Const.EMERGENCY_CONTACT_HUMAN_NAME, "")
        val emergencyLastName = Hawk.get(Const.EMERGENCY_CONTACT_HUMAN_LAST_NAME, "")
        val emergencyFullName = "$emergencyName $emergencyLastName"
        val emergencyEmail = Hawk.get(Const.EMERGENCY_CONTACT_HUMAN_EMAIL, "")
        val fullText = "<p>${context.getString(R.string.email_1)}</p>" +
                "<p>${context.getString(R.string.email_2)}</p>" +
                "<p>${context.getString(R.string.email_3, yourFullName, from)}</p>" +
                "<p>${context.getString(R.string.email_4, location)}</p>" +
                "<p>${context.getString(R.string.email_5)}<br>" +
                "${context.getString(R.string.email_6)}<br>" +
                "${context.getString(R.string.email_7)}</p>"

        val subject = context.getString(R.string.email_emergency_message)

        RetrofitClient.retrofitService.sendEmail(
            toContactName = emergencyFullName,
            toContactAddress = emergencyEmail,
            text = fullText,
            subject = subject
        )
            .enqueue(object : Callback<Int> {
                override fun onFailure(call: Call<Int>, t: Throwable) {
                    Log.d("TEST_TEST", "send email FAIL")
                }

                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    Log.d("TEST_TEST", "send email OK")
                }
            }
            )
    }

    override fun sendEmailTracking(location: String) {
        val yourName = Hawk.get(Const.HUMAN_NAME, "")
        val yourLastName = Hawk.get(Const.HUMAN_LAST_NAME, "")
        val yourFullName = "$yourName $yourLastName"
        val emergencyName = Hawk.get(Const.EMERGENCY_CONTACT_HUMAN_NAME, "")
        val emergencyLastName = Hawk.get(Const.EMERGENCY_CONTACT_HUMAN_LAST_NAME, "")
        val emergencyFullName = "$emergencyName $emergencyLastName"
        val emergencyEmail = Hawk.get(Const.EMERGENCY_CONTACT_HUMAN_EMAIL, "")
        val fullText = "<p>${context.getString(R.string.email_1)}</p>" +
                "<p>${context.getString(R.string.email_2)}</p>" +
                "<p>${context.getString(R.string.email_track_3, yourFullName)}</p>" +
                "<p>${context.getString(R.string.email_4, location)}</p>" +
                "<p>${context.getString(R.string.email_5)}<br>" +
                "${context.getString(R.string.email_6)}<br>" +
                "${context.getString(R.string.email_7)}</p>"

        val subject = context.getString(R.string.email_emergency_message)

        RetrofitClient.retrofitService.sendEmail(
            toContactName = emergencyFullName,
            toContactAddress = emergencyEmail,
            text = fullText,
            subject = subject
        )
            .enqueue(object : Callback<Int> {
                override fun onFailure(call: Call<Int>, t: Throwable) {
                    Log.d("TEST_TEST", "send email FAIL")
                }

                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    Log.d("TEST_TEST", "send email OK")
                }
            }
            )
    }
}