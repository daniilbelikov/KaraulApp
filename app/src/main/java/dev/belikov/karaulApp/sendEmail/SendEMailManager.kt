package dev.belikov.karaulApp.sendEmail

import android.content.Context
import dev.belikov.karaulApp.utils.SingletonHolder

class SendEMailManager private constructor(val context: Context) {

    var sendEmailHelper: SendEmailHelper? = null

    init {
        sendEmailHelper = SendEmailHelper(context)
    }

    companion object : SingletonHolder<SendEMailManager, Context>(::SendEMailManager)

}