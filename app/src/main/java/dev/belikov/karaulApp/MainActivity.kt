package dev.belikov.karaulApp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.orhanobut.hawk.Hawk
import dev.belikov.karaulApp.utils.Const
import dev.belikov.karaulApp.utils.Const.ACCESS_COARSE_LOCATION_REQUEST_CODE
import dev.belikov.karaulApp.utils.Const.ACCESS_FINE_LOCATION_REQUEST_CODE
import dev.belikov.karaulApp.utils.Const.DONATION_COMPLETE
import java.util.*

class MainActivity : AppCompatActivity() {

    private var currentNavController: NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Hawk.init(applicationContext).build()
        Hawk.put(DONATION_COMPLETE, true)
        if (checkPermissions()) {
            initView()
        }

    }

    private fun initView() {
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_nav)

        val navController =
            Navigation.findNavController(this@MainActivity, R.id.nav_host)
        val graph = navController.navInflater.inflate(R.navigation.nav_graph)
        navController.graph = graph
        bottomNav.setupWithNavController(navController)
        currentNavController = navController
    }

    private fun checkPermissions(): Boolean {
        return (checkSelfPermission(
            Manifest.permission.ACCESS_FINE_LOCATION,
            ACCESS_FINE_LOCATION_REQUEST_CODE
        )
                && checkSelfPermission(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Const.ACCESS_COARSE_LOCATION_REQUEST_CODE
        ))
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(permission),
                requestCode
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= 23) {
            when (requestCode) {
//                CALL_PHONE_REQUEST_CODE -> if (grantResults.isNotEmpty()
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                ) {
//                    requestPermissions(
//                        arrayOf(Manifest.permission.CALL_PHONE),
//                        CALL_PHONE_REQUEST_CODE
//                    )
//                }
//                READ_CONTACTS_REQUEST_CODE ->
//                    if (grantResults.isNotEmpty()
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                    ) {
//                        requestPermissions(
//                            arrayOf(Manifest.permission.READ_CONTACTS),
//                            READ_CONTACTS_REQUEST_CODE
//                        )
//                    }

                ACCESS_FINE_LOCATION_REQUEST_CODE ->
                    if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    ) {

                        requestPermissions(
                            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                            ACCESS_COARSE_LOCATION_REQUEST_CODE
                        )
                    } else {
                        finish()
                    }
                ACCESS_COARSE_LOCATION_REQUEST_CODE ->
                    if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    ) {
                        initView()
                    } else {
                        finish()
                    }

            }
        }
    }
}