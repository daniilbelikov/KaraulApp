package dev.belikov.karaulApp.dialog

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.orhanobut.hawk.Hawk
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.databinding.DialogContactBinding
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_EMAIL
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_LAST_NAME
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_MESSAGE
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_NAME
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_PHONE
import dev.belikov.karaulApp.utils.Const.HUMAN_LAST_NAME
import dev.belikov.karaulApp.utils.Const.HUMAN_NAME
import dev.belikov.karaulApp.utils.Const.HUMAN_PHONE
import dev.belikov.karaulApp.utils.Const.PHONE_MASK_RUS
import dev.belikov.karaulApp.utils.Const.READ_CONTACTS_REQUEST_CODE
import dev.belikov.karaulApp.utils.ContactPicker
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.FormatWatcher
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

class ContactDialogFragment : BottomSheetDialogFragment() {

    private lateinit var binding: DialogContactBinding

    private val args: ContactDialogFragmentArgs by navArgs()

    private val picker: ContactPicker? by lazy {
        ContactPicker.create(
            activity = requireActivity() as AppCompatActivity,
            onContactPicked = {
                binding.contactNameTie.setText(it.name)
                binding.contactLastnameTie.setText(it.lastName)
                binding.contactPhoneTie.setText(it.phoneNumber)
                binding.emergencyContactEmailTie.setText(it.email)
            },
            onFailure = {
                Log.d("TAG", it.message ?: "Unknown error")
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_contact,
            container,
            false,
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val isEmergency = args.isEmergency

        if (!isEmergency) {
            binding.apply {
                contactLastnameTie.setText(Hawk.get(HUMAN_NAME, ""))
                contactLastnameTie.setText(Hawk.get(HUMAN_LAST_NAME, ""))
                contactPhoneTie.setText(Hawk.get(HUMAN_PHONE, ""))
                emergencyContactEmailTie.isVisible = false
                emergencyContactEmailIv.isVisible = false
                emergencyContactEmailTv.isVisible = false
                emergencyContactEmailMessageTie.isVisible = false
                emergencyContactEmailMessageIv.isVisible = false
                emergencyContactEmailMessageTv.isVisible = false
                emergencyContactPickContactTv.isVisible = false
            }
        } else {
            binding.apply {
                contactNameTie.setText(Hawk.get(EMERGENCY_CONTACT_HUMAN_NAME, ""))
                contactLastnameTie.setText(Hawk.get(EMERGENCY_CONTACT_HUMAN_LAST_NAME, ""))
                contactPhoneTie.setText(Hawk.get(EMERGENCY_CONTACT_HUMAN_PHONE, ""))
                emergencyContactEmailTie.setText(Hawk.get(EMERGENCY_CONTACT_HUMAN_EMAIL, ""))
                emergencyContactEmailMessageTie.setText(
                    Hawk.get(
                        EMERGENCY_CONTACT_HUMAN_MESSAGE,
                        ""
                    )
                )
                emergencyContactPickContactTv.setOnClickListener {
                    if (ContextCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.READ_CONTACTS
                        )
                        != PackageManager.PERMISSION_GRANTED
                    ) {
                        requestPermissions(
                            arrayOf(Manifest.permission.READ_CONTACTS),
                            READ_CONTACTS_REQUEST_CODE
                        )
                    } else {
                        picker?.pick()
                    }
                }
            }

        }

        binding.emergencyContactSaveBtn.setOnClickListener {
            if (isEmergency) {
                if (isCorrectContentContact(isEmergency)) {
                    saveEmergencyContact()
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "emergencyContactSave", isEmergency
                    )
                    findNavController().navigateUp()
                } else {
                    showIncorrectInformationDialog(isEmergency)
                }
            } else {
                if (isCorrectContentContact(isEmergency)) {
                    saveContact()
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "emergencyContactSave", isEmergency
                    )
                    findNavController().navigateUp()
                } else {
                    showIncorrectInformationDialog(isEmergency)
                }
            }
        }

        val slots = UnderscoreDigitSlotsParser().parseSlots(PHONE_MASK_RUS) //111-111-1111
        val formatWatcher: FormatWatcher = MaskFormatWatcher(
            MaskImpl.createTerminated(slots)
        )
        formatWatcher.installOn(binding.contactPhoneTie)


    }

    private fun isCorrectContentContact(isEmergency: Boolean): Boolean {
        binding.apply {
            if (contactNameTie.text.toString().isEmpty())
                return false
            if (contactLastnameTie.text.toString().isEmpty())
                return false
            if (contactPhoneTie.text.toString().isEmpty())
                return false
            if (isEmergency) {
                if (emergencyContactEmailTie.text.toString().isEmpty())
                    return false
                if (emergencyContactEmailMessageTie.text.toString().isEmpty())
                    return false
            }
            return true
        }
    }

    private fun showIncorrectInformationDialog(isEmergency: Boolean) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.alert_dialog_title))
            .setMessage(
                getString(
                    if (isEmergency)
                        R.string.alert_dialog_message_emergency_contact
                    else
                        R.string.alert_dialog_message_contact
                )
            )
        builder.setPositiveButton(
            getString(R.string.alert_dialog_ok)
        ) { dialog, _ ->
            dialog.cancel()
        }

        builder.create().show()
    }

    private fun saveContact() {
        binding.apply {
            Hawk.put(HUMAN_NAME, contactNameTie.text.toString())
            Hawk.put(HUMAN_LAST_NAME, contactLastnameTie.text.toString())
            Hawk.put(HUMAN_PHONE, contactPhoneTie.text.toString())
        }
    }

    private fun saveEmergencyContact() {
        binding.apply {
            Hawk.put(EMERGENCY_CONTACT_HUMAN_NAME, contactNameTie.text.toString())
            Hawk.put(EMERGENCY_CONTACT_HUMAN_LAST_NAME, contactLastnameTie.text.toString())
            Hawk.put(EMERGENCY_CONTACT_HUMAN_PHONE, contactPhoneTie.text.toString())
            Hawk.put(EMERGENCY_CONTACT_HUMAN_EMAIL, emergencyContactEmailTie.text.toString())
            Hawk.put(
                EMERGENCY_CONTACT_HUMAN_MESSAGE,
                emergencyContactEmailMessageTie.text.toString()
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            READ_CONTACTS_REQUEST_CODE -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                picker?.pick()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
                ) {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.need_read_contacts),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.denied_read_contacts),
                        Toast.LENGTH_LONG
                    ).show()
//                        .setAction("тест-тест") {
//                            startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
//                                data = Uri.fromParts("package", requireActivity().packageName, null)
//                            })
//                        }.show()
                }
            }
        }
    }
}