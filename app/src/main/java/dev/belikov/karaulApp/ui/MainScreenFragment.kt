package dev.belikov.karaulApp.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.databinding.FragmentMainBinding
import dev.belikov.karaulApp.sendEmail.SendEMailManager
import dev.belikov.karaulApp.sendEmail.SendEmailHelper
import dev.belikov.karaulApp.utils.Const.DONATION_COMPLETE
import dev.belikov.karaulApp.utils.Const.FULL_VISION
import dev.belikov.karaulApp.utils.Const.HALF_VISION

class MainScreenFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private var sendEmailHelper: SendEmailHelper? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_main,
            container,
            false,
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val phoneNumbersList = resources.getStringArray(R.array.phone_numbers)
        val emergencyNameList = resources.getStringArray(R.array.emergency_name)
        sendEmailHelper = SendEMailManager.getInstance(requireContext()).sendEmailHelper
        setListeners(phoneNumbersList)
        setTexts(phoneNumbersList, emergencyNameList)
        checkDonation()
    }

    private fun checkDonation() {
        val donation = Hawk.get(DONATION_COMPLETE, false)
        binding.apply {
            if (!donation) {
                callSosCard.alpha = HALF_VISION
                smsSosCard.alpha = HALF_VISION
                callSosCard.setOnClickListener {
                    findNavController().navigate(MainScreenFragmentDirections.goToDonation())
                }
                smsSosCard.setOnClickListener {
                    findNavController().navigate(MainScreenFragmentDirections.goToDonation())
                }
            } else {
                callSosCard.setOnClickListener {
                    Toast.makeText(requireContext(), "Test-TEST", Toast.LENGTH_LONG).show()
                }
                smsSosCard.setOnClickListener {
                    Toast.makeText(requireContext(), "Test-TEST", Toast.LENGTH_LONG).show()
                }
                callSosCard.alpha = FULL_VISION
                smsSosCard.alpha = FULL_VISION
            }
        }
    }

    private fun setTexts(phoneNumbersList: Array<String>, emergencyNameList: Array<String>) {
        binding.apply {
            fireNumberTv.text = phoneNumbersList[0]
            policeNumberTv.text = phoneNumbersList[1]
            medicNumberTv.text = phoneNumbersList[2]
            gasNumberTv.text = phoneNumbersList[3]
            fastCallPhoneNumberTv.text = phoneNumbersList[4]

            fireTv.text = emergencyNameList[0]
            policeTv.text = emergencyNameList[1]
            medicTv.text = emergencyNameList[2]
            gasTv.text = emergencyNameList[3]
            callSosTv.text = emergencyNameList[4]
            smsSosTv.text = emergencyNameList[5]
        }

    }

    private fun setListeners(phoneNumbersList: Array<String>) {
        binding.apply {
            fireCard.setOnClickListener {
                callPhone(phoneNumbersList[0])
                sendEmailHelper?.sendEmail(getString(R.string.email_fire))
            }
            policeCard.setOnClickListener {
                callPhone(phoneNumbersList[1])
                sendEmailHelper?.sendEmail(getString(R.string.email_police))
            }
            medicCard.setOnClickListener {
                callPhone(phoneNumbersList[2])
                sendEmailHelper?.sendEmail(getString(R.string.email_medic))
            }
            gasCard.setOnClickListener {
                callPhone(phoneNumbersList[3])
                sendEmailHelper?.sendEmail(getString(R.string.email_gas))
            }
            fastCallBtn.setOnClickListener {
                callPhone(phoneNumbersList[4])
                sendEmailHelper?.sendEmail(getString(R.string.email_fast_call))
            }
        }
    }

    private fun callPhone(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        startActivity(intent)
    }

}