package dev.belikov.karaulApp.ui

import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.orhanobut.hawk.Hawk
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.databinding.FragmentMapsBinding
import dev.belikov.karaulApp.location.LocationHelper
import dev.belikov.karaulApp.location.LocationManager
import dev.belikov.karaulApp.sendEmail.SendEMailManager
import dev.belikov.karaulApp.sendEmail.SendEmailHelper
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_PHONE
import java.util.*

class MapsScreenFragment : Fragment() {

    private lateinit var googleMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var sendEmailHelper: SendEmailHelper? = null

    private var locationHelper: LocationHelper? = null

    private var bigCircle: Circle? = null
    private var smallCircle: Circle? = null

    private lateinit var placesClient: PlacesClient

    private lateinit var binding: FragmentMapsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_maps,
            container,
            false,
        )

        return binding.root
    }


    private val callback = OnMapReadyCallback { googleMapCallback ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        googleMapCallback.uiSettings.isMyLocationButtonEnabled = false
        googleMap = googleMapCallback

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Places.initialize(requireContext(), resources.getString(R.string.google_maps_key))
        placesClient = Places.createClient(requireContext())
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity());
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
        sendEmailHelper = SendEMailManager.getInstance(requireContext()).sendEmailHelper
        locationHelper =
            LocationManager.getInstance(requireContext().applicationContext).locationHelper
        initView()
        checkLocation()
//        checkDonation()
    }

    private fun initView() {
        binding.firstBtn.checkTimer()
        binding.firstBtn.setOnClickListener {
            sendEmailHelper?.sendEmailTimer()
            it.checkTimer()
        }
        binding.secondBtn.setOnClickListener {
            checkLocation(true)
        }
    }

    private fun View.checkTimer() {
        this.setBackgroundColor(
            resources.getColor(
                if (sendEmailHelper?.checkTimer() == true)
                    R.color.big_circle_stroke_color else R.color.save_btn
            )
        )

    }

    private fun checkLocation(isSendSMS: Boolean = false) {
        val locationResult: Task<Location>? = locationHelper?.getLocation()
        locationResult?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val location = task.result
                location?.let { it ->
                    val currentLocation = LatLng(it.latitude, it.longitude)
                    if (isSendSMS) {
                        sendSms(currentLocationString = getPositionName(currentLocation))
                    } else {
                        getDeviceLocation(currentLocation)
                    }
                } ?: run {
                    if (isSendSMS) {
                        sendSms(currentLocationString = "Fail find location")
                    }
                }
            }
        }
    }

    private fun sendSms(currentLocationString: String) {
        val smsTo = Hawk.get(EMERGENCY_CONTACT_HUMAN_PHONE, "")
        val smsText = resources.getString(R.string.sms_text, currentLocationString)
        val uri: Uri = Uri.parse("smsto:$smsTo")
        val intent = Intent(Intent.ACTION_SENDTO, uri)
        intent.putExtra("sms_body", smsText)
        startActivity(intent)
    }


    private fun getDeviceLocation(locationLatLng: LatLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, 17.2f))
        bigCircle?.let { circle -> circle.remove() }
        smallCircle?.let { circle -> circle.remove() }
        bigCircle = googleMap.addCircle(
            CircleOptions()
                .center(locationLatLng)
                .radius(35.0)
                .strokeColor(resources.getColor(R.color.big_circle_stroke_color))
                .strokeWidth(0.0f)
                .fillColor(resources.getColor(R.color.big_circle_fill_color))
        )
        smallCircle = googleMap.addCircle(
            CircleOptions()
                .center(locationLatLng)
                .radius(6.0)
                .strokeColor(resources.getColor(R.color.small_circle_stroke_color))
                .strokeWidth(2.5f)
                .fillColor(resources.getColor(R.color.small_circle_fill_color))
        )
        binding.positionTv.text = getPositionName(locationLatLng)
    }

    private fun getPositionName(locationLatLng: LatLng): String {
        val geocoder = Geocoder(requireContext(), Locale.getDefault())
        return geocoder.getFromLocation(
            locationLatLng.latitude,
            locationLatLng.longitude,
            1
        )[0].getAddressLine(0)

    }
}