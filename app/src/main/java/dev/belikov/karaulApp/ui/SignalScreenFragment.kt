package dev.belikov.karaulApp.ui

import android.content.Context.AUDIO_SERVICE
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.databinding.FragmentSignalBinding


class SignalScreenFragment : Fragment() {

    private lateinit var binding: FragmentSignalBinding

    private var handlerAnimation = Handler()
//    private var statusAnimation = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_signal,
            container,
            false,
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        startPulse()
    }


    private fun initViews() {
        val mediaPlayer = MediaPlayer.create(requireContext(), R.raw.alarm)
        mediaPlayer.setVolume(1.0f, 1.0f)

        binding.signalBtn.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.stop()
                mediaPlayer.prepare()
            } else {
                setupMaxVolume()
                mediaPlayer.start()
                sendEmail()
            }
        }
    }

    private fun sendEmail() {
        Toast.makeText(requireContext(), "Send email", Toast.LENGTH_LONG).show()
    }

    private fun setupMaxVolume() {
        val audioManger: AudioManager = activity?.getSystemService(AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= 23) {
            audioManger.setStreamVolume(
                AudioManager.STREAM_SYSTEM,
                audioManger.getStreamMaxVolume(AudioManager.STREAM_SYSTEM),
                AudioManager.FLAG_PLAY_SOUND
            )
            audioManger.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                audioManger.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_PLAY_SOUND
            )
            audioManger.setStreamVolume(
                AudioManager.STREAM_RING,
                audioManger.getStreamMaxVolume(AudioManager.STREAM_RING),
                AudioManager.FLAG_PLAY_SOUND
            )
        } else {
            audioManger.setStreamVolume(
                AudioManager.STREAM_SYSTEM,
                audioManger.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                0
            )
            audioManger.setStreamVolume(
                AudioManager.STREAM_RING,
                audioManger.getStreamMaxVolume(AudioManager.STREAM_RING),
                0
            )

            audioManger.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                audioManger.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                0
            )
        }
    }

    private fun startPulse() {
        runnable.run()
    }

    private fun stopPulse() {
        handlerAnimation.removeCallbacks(runnable)
    }

    private var runnable = object : Runnable {
        override fun run() {
            binding.imgAnimation1.animate().scaleX(4f).scaleY(4f).alpha(0f).setDuration(3000)
                .withEndAction {
                    binding.imgAnimation1.scaleX = 1f
                    binding.imgAnimation1.scaleY = 1f
                    binding.imgAnimation1.alpha = 1f
                }

            binding.imgAnimation2.animate().scaleX(4f).scaleY(4f).alpha(0f).setDuration(4000)
                .withEndAction {
                    binding.imgAnimation2.scaleX = 1f
                    binding.imgAnimation2.scaleY = 1f
                    binding.imgAnimation2.alpha = 1f
                }

            binding.imgAnimation3.animate().scaleX(4f).scaleY(4f).alpha(0f).setDuration(5000)
                .withEndAction {
                    binding.imgAnimation3.scaleX = 1f
                    binding.imgAnimation3.scaleY = 1f
                    binding.imgAnimation3.alpha = 1f
                }
            binding.imgAnimation4.animate().scaleX(4f).scaleY(4f).alpha(0f).setDuration(6000)
                .withEndAction {
                    binding.imgAnimation4.scaleX = 1f
                    binding.imgAnimation4.scaleY = 1f
                    binding.imgAnimation4.alpha = 1f
                }

            handlerAnimation.postDelayed(this, 6500)
        }
    }


}