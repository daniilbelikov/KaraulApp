package dev.belikov.karaulApp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.databinding.FragmentDonationBinding

class DonationFragment : Fragment() {

    private lateinit var binding: FragmentDonationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_donation,
            container,
            false,
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            closeTv.setOnClickListener {
                findNavController().navigateUp()
            }
            donationBtn.setOnClickListener {
                Toast.makeText(requireContext(), "Test-test-test", Toast.LENGTH_LONG).show()
            }
        }
    }
}