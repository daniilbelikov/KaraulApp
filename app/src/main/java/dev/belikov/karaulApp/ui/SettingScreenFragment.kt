package dev.belikov.karaulApp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import dev.belikov.karaulApp.R
import dev.belikov.karaulApp.databinding.FragmentSettingsBinding
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_EMAIL
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_LAST_NAME
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_MESSAGE
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_NAME
import dev.belikov.karaulApp.utils.Const.EMERGENCY_CONTACT_HUMAN_PHONE
import dev.belikov.karaulApp.utils.Const.HUMAN_LAST_NAME
import dev.belikov.karaulApp.utils.Const.HUMAN_NAME
import dev.belikov.karaulApp.utils.Const.HUMAN_PHONE
import dev.belikov.karaulApp.utils.Const.PHONE_MASK_RUS
import dev.belikov.karaulApp.utils.Const.SEND_EMAIL
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.FormatWatcher
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

class SettingScreenFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_settings,
            container,
            false,
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        binding.testPaySw.isVisible = false
//        binding.testPayTv.isVisible = false

        setHumanData()
        setEmergencyHumanData()

        val slots = UnderscoreDigitSlotsParser().parseSlots(PHONE_MASK_RUS)
        val formatWatcher: FormatWatcher = MaskFormatWatcher(
            MaskImpl.createTerminated(slots)
        )
        formatWatcher.installOn(binding.humanPhoneTv)
        formatWatcher.installOn(binding.emergencyContactPhoneTv)

        setListeners()

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Boolean>("emergencyContactSave")
            ?.observe(viewLifecycleOwner) { result ->
                if (result) setEmergencyHumanData() else setHumanData()
            }
    }

    private fun setListeners() {
        binding.editHumanTv.setOnClickListener {
            findNavController().navigate(
                SettingScreenFragmentDirections.goToContactDialog(
                    false
                )
            )
        }
        binding.editEmergencyContactTv.setOnClickListener {
            findNavController().navigate(
                SettingScreenFragmentDirections.goToContactDialog(
                    true
                )
            )
        }
        binding.anotherOptionsSendEmailSw.isChecked = Hawk.get(SEND_EMAIL, false)

        binding.anotherOptionsSendEmailSw.setOnCheckedChangeListener { _, isChecked ->
            Hawk.put(SEND_EMAIL, isChecked)
        }

//        binding.testPaySw.isChecked = Hawk.get(DONATION_COMPLETE, false)
//
//        binding.testPaySw.setOnCheckedChangeListener { _, isChecked ->
//            Hawk.put(DONATION_COMPLETE, isChecked)
//        }
    }

    private fun setEmergencyHumanData() {
        binding.apply {
            val name = setInfo(Hawk.get(EMERGENCY_CONTACT_HUMAN_NAME, ""))
            val lastName = setInfo(Hawk.get(EMERGENCY_CONTACT_HUMAN_LAST_NAME, ""))
            val phone = setInfoPhone(Hawk.get(EMERGENCY_CONTACT_HUMAN_PHONE, ""))
            val email = setInfo(Hawk.get(EMERGENCY_CONTACT_HUMAN_EMAIL, ""))
            val emailMessage = setInfo(Hawk.get(EMERGENCY_CONTACT_HUMAN_MESSAGE, ""))
            val fullName = createFullName(name, lastName)

            emergencyContactNameTv.text = fullName
            emergencyContactPhoneTv.text = phone
            emergencyContactEmailTv.text = email
            emergencyContactMessageTv.text = emailMessage
        }
    }

    private fun setHumanData() {
        val name = setInfo(Hawk.get(HUMAN_NAME, ""))
        val lastName = setInfo(Hawk.get(HUMAN_LAST_NAME, ""))
        val phone = setInfoPhone(Hawk.get(HUMAN_PHONE, ""))
        val fullName = createFullName(name, lastName)
        binding.humanNameTv.text = fullName
        binding.humanPhoneTv.text = phone
    }

    private fun setInfo(text: String): String {
        val noInformation = getString(R.string.no_information)
        return if (text.isEmpty()) {
            noInformation
        } else {
            text
        }
    }

    private fun setInfoPhone(text: String): String {
        val noInformation = getString(R.string.no_information)
        return if (text.isEmpty()) {
            noInformation
        } else {
            text
        }
    }

    private fun createFullName(name: String, lastName: String): String {
        val noInformation = getString(R.string.no_information)
        return if (name == noInformation && lastName == noInformation) {
            noInformation
        } else {
            "$name $lastName"
        }
    }


}