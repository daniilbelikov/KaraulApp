package dev.belikov.karaulApp.location

import android.content.Context
import dev.belikov.karaulApp.utils.SingletonHolder


class LocationManager private constructor(context: Context) {
    var locationHelper: LocationHelper? = null

    init {
        locationHelper = LocationHelper(context)
    }

    companion object : SingletonHolder<LocationManager, Context>(::LocationManager)
}
