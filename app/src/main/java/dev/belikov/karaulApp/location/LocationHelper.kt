package dev.belikov.karaulApp.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.location.Location
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import java.util.*

class LocationHelper(val context: Context) {
    private var fusedLocationProviderClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    @SuppressLint("MissingPermission")
    fun getCurrentLocation(locationCallback: LocationInterface, from: String? = null) {
        val locationResult: Task<Location> = fusedLocationProviderClient.lastLocation
        locationResult.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val location = task.result
                location?.let { it ->
                    val currentLocation = LatLng(it.latitude, it.longitude)
                    from?.let {
                        locationCallback.sendEmail(getLocationText(currentLocation), it)
                    } ?: run {
                        locationCallback.sendEmailTracking(getLocationText(currentLocation))
                    }
                } ?: run {
                    from?.let {
                        secondVariantLocation(locationCallback, it)
                    } ?: run {
                        secondVariantLocation(locationCallback)
                    }
                }
            } else {
                from?.let {
                    secondVariantLocation(locationCallback, it)
                } ?: run {
                    secondVariantLocation(locationCallback)
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun getLocation(): Task<Location>? =
        fusedLocationProviderClient.lastLocation

    private fun getLocationText(currentLocation: LatLng): String {
        val geocoder = Geocoder(context, Locale.getDefault())
        return geocoder.getFromLocation(
            currentLocation.latitude,
            currentLocation.longitude,
            1
        )[0].getAddressLine(0)
    }

    @SuppressLint("MissingPermission")
    private fun secondVariantLocation(locationCallback: LocationInterface, from: String? = null) {
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult == null) {
                    from?.let { from ->
                        locationCallback.sendEmail("Fail find location", from)
                    } ?: run {
                        locationCallback.sendEmailTracking("Fail find location")
                    }
                }
                val location = locationResult.locations[0]
                location?.let {
                    val currentLocation = LatLng(it.latitude, it.longitude)
                    from?.let { from ->
                        locationCallback.sendEmail(getLocationText(currentLocation), from)
                    } ?: run {
                        locationCallback.sendEmailTracking(getLocationText(currentLocation))
                    }
                }
            }
        }

        LocationServices.getFusedLocationProviderClient(context)
            .requestLocationUpdates(mLocationRequest, mLocationCallback, null)
    }

}