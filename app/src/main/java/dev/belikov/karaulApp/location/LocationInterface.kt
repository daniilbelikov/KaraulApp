package dev.belikov.karaulApp.location

interface LocationInterface {
    fun sendEmail(location: String, from: String)
    fun sendEmailTracking(location: String)
}