package dev.belikov.karaulApp.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface KaraulApiService {
    @GET("karaul_send_email_feedback.php")
    fun sendEmail(
        @Query("toContactName") toContactName: String,
        @Query("toContactAddress") toContactAddress: String,
        @Query("text") text: String,
        @Query("subject") subject: String
    ): Call<Int>
}